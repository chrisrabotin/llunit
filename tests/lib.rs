extern crate llunit;

use llunit::prelude::*;
use std::f64::EPSILON;

#[test]
fn basic_distance() {
    let mm_10000 = Distance::from(1000.0, Meter::milli);
    assert!((mm_10000.to_base() - 1.0).abs() < EPSILON);
    // Convert to km
    assert!((mm_10000.to(Meter::kilo) - 1e-3).abs() < EPSILON);
    // Convert to gm
    assert!((mm_10000.to(Meter::giga) - 1e-9).abs() < EPSILON);
    // Convert to pm
    assert!((mm_10000.to(Meter::pico) - 1e12).abs() < EPSILON);

    // Try negative
    let mm_10000 = Distance::from(-1000.0, Meter::milli);
    assert!((mm_10000.to_base() + 1.0).abs() < EPSILON);
    // Convert to km
    assert!((mm_10000.to(Meter::kilo) + 1e-3).abs() < EPSILON);
    // Convert to gm
    assert!((mm_10000.to(Meter::giga) + 1e-9).abs() < EPSILON);
    // Convert to pm
    assert!((mm_10000.to(Meter::pico) + 1e12).abs() < EPSILON);
}

#[test]
fn lossless_distance() {
    let one_third = Distance::from_fraction(1, 3, Meter::base);
    let two_third = Distance::from_fraction(2, 3, Meter::base);

    println!("{:.1e}", two_third);

    assert!(((one_third + two_third).to_base() - 1.0).abs() < EPSILON);

    assert!(((one_third - two_third).to_base() + one_third.to_base()).abs() < EPSILON);

    // From application example
    let base = Distance::from(3.919_868_89e5, Meter::kilo);
    let offset_base = Distance::from(20.0 / 3.0, Meter::kilo);
    let offset = Distance::from_decimal(
        offset_base.decimal() * Decimal::from(-0.236_050_70),
        Meter::base,
    );

    let askmf64 = 3.919_868_89e5 + (20.0 / 3.0) * -0.236_050_70e-3;

    // TODO: Something is wrong in the -0.236... it isn't added correctly.
    println!("{}\n{}", base + offset, askmf64);
}

#[test]
fn print_distance() {
    assert_eq!(format!("{}", Distance::from(1000.0, Meter::milli)), "1 m");
    assert_eq!(format!("{}", Distance::from(1.0, Meter::milli)), "1 mm");
    assert_eq!(format!("{}", Distance::from(1.0, Meter::kilo)), "1000 m");
    assert_eq!(format!("{}", Distance::from(5.0, Meter::kilo)), "5000 m");
    assert_eq!(format!("{}", Distance::from(10.1, Meter::kilo)), "10.1 km");
    assert_eq!(
        format!("{}", Distance::from(1680.0, Meter::milli)),
        "1.68 m"
    );

    // Test negative with exponent
    assert_eq!(
        format!("{:.3e}", Distance::from(-1000.0, Meter::milli)),
        "-1.000e0 m"
    );
    assert_eq!(
        format!("{:.3e}", Distance::from(-1.0, Meter::milli)),
        "-1.000e0 mm"
    );
    assert_eq!(
        format!("{:.3e}", Distance::from(-1.0, Meter::kilo)),
        "-1.000e3 m"
    );
    assert_eq!(
        format!("{:.3e}", Distance::from(-5.0, Meter::kilo)),
        "-5.000e3 m"
    );
    assert_eq!(
        format!("{:.3e}", Distance::from(-10.1, Meter::kilo)),
        "-1.010e1 km"
    );
    assert_eq!(
        format!("{:.3e}", Distance::from(-1680.0, Meter::milli)),
        "-1.680e0 m"
    );
}

#[test]
fn easy_funcs() {
    let increase_speed = |d: Distance| d + Distance::from_base(10.0);

    assert!(
        (increase_speed(Distance::from_base(-10.0)) - Distance::from_base(10.0)).to_base()
            < EPSILON
    );
}
