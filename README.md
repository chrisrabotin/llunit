# hifiunit
A unit of measurement management library which preserves precision and makes it trivial to interact with human inputs.

# Requirements
Here be a list of requirements of the library, whether those are implemented, and a link to the relevant test.


|Req.  | Description  | Implemented  | Test  |
|---------|---------|---------|---------|
| Lossless unit type |  Unit should be stored lossless where possible to avoid any loss of precision during calculations | Yes | N/A |
| Convert from string to Unit | Library is meant for human usage, so this is important | NO | N/A |
| Add and sub with same dimensions | Losselessness should enable calculations | NO | N/A |
| Mul and div with dimension change | Losselessness should enable calculations | NO | N/A |
| Convert to different primitive types | User might want the data as an i32 instead of f64 | NO | N/A |
| Convert to different units | Whatever the initial units were it must be possible to convert to another unit of the same dimension | NO | N/A |
| Display impl for all units | Humans will read the units | NO | N/A |
| Display impl with specified unit | Humans will read the units | NO | N/A |
| Trivial initialization without string | If a user knows what units they are storing the data as, they should trivially support units | NO | N/A |
| Support SI distances | I have a use case of distances ([nyx](https://gitlab.com/chrisrabotin/nyx)) | NO | N/A |
| Support SI velocities | I have a use case of velocities ([nyx](https://gitlab.com/chrisrabotin/nyx)) | NO | N/A |
| Support SI acceleration | I have a use case of accelerations ([nyx](https://gitlab.com/chrisrabotin/nyx)) | NO | N/A |
| Support from femtoseconds to days | I have a use case for time ([hifitime](https://github.com/ChristopherRabotin/hifitime)) | NO | N/A |


# Example usage

```
extern crate llunit;

use llunit;

fn speed_with_unit(distance: Unit<Distance>, timespan: Unit<Time>) -> Unit<Velocity> {
    distance / timespan
}

fn speed_from_str_to_f64(distance: String, timespan: String) -> Result<f64, UnitError> {
    // The unit is automatically parsed and stored as a lossless decimal
    // so there isn't any loss of precision.
    let dist_unit = Unit::<Distance>::try_from(distance)?;
    let ts_unit: Unit<Time> = timespan.try_into())?;

    // We now have a speed with some units (doesn't really matter at this stage)
    let speed = speed_with_unit(dist_unit, ts_unit);
    Ok(speed.to_f64("km/s".to_string()));
}

```