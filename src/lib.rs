extern crate fraction;

use fraction::{GenericDecimal, GenericFraction};

#[macro_use]
mod macros;
mod distance;

pub mod prelude {
    pub use super::Decimal;
    pub use crate::distance::*;
}

#[derive(Debug)]
pub enum UnitError {
    NotRepresentable(String),
}

/// We're using the highest possible precision for fractions and for comparisons.
pub type Decimal = GenericDecimal<u128, u16>;
type Fraction = GenericFraction<u128>;

#[derive(Clone, Copy, Debug)]
#[allow(non_camel_case_types)]
pub enum Exponent {
    yotta,
    zetta,
    exa,
    peta,
    tera,
    giga,
    mega,
    kilo,
    hecto,
    deca,
    base,
    deci,
    centi,
    milli,
    micro,
    nano,
    pico,
    femto,
    atto,
    zepto,
    yocto,
}

impl Exponent {
    /// Returns the value of the exponent
    pub fn value(&self) -> i32 {
        match self {
            Exponent::yotta => 24,
            Exponent::zetta => 21,
            Exponent::exa => 18,
            Exponent::peta => 15,
            Exponent::tera => 12,
            Exponent::giga => 9,
            Exponent::mega => 6,
            Exponent::kilo => 3,
            Exponent::hecto => 2,
            Exponent::deca => 1,
            Exponent::base => 0,
            Exponent::deci => -1,
            Exponent::centi => -2,
            Exponent::milli => -3,
            Exponent::micro => -6,
            Exponent::nano => -9,
            Exponent::pico => -12,
            Exponent::femto => -15,
            Exponent::atto => -18,
            Exponent::zepto => -21,
            Exponent::yocto => -24,
        }
    }

    /// Returns the conversion from the exponent to the decimal, i.e. 10^-{exponent}.
    pub fn to_base_unit(&self) -> Decimal {
        Decimal::from(10.0_f64.powi(self.value()))
    }

    /// Returns the conversion from the base unit to the provided exponent to the decimal, i.e. 10^{exponent}.
    pub fn from_base_unit(&self) -> Decimal {
        Decimal::from(10.0_f64.powi(-self.value()))
    }
}

/// Represents a time span, base unit in seconds.
pub struct Time(Decimal);

/// Represents a velocity, base unit is meters per second (m/s).
pub struct Velocity(distance::Distance, Time);

impl_dim_ops!(Time);
