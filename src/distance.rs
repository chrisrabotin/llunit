use super::fraction::ToPrimitive;
use super::{Decimal, Exponent, Fraction, UnitError};
use std::fmt;

pub type Meter = Exponent;

/// Represents a distance, base unit is meters.
#[derive(Clone, Copy, Debug)]
pub struct Distance(Decimal);

impl Distance {
    pub fn from_decimal(d: Decimal, expo: Exponent) -> Self {
        Self(d * expo.to_base_unit())
    }

    /// Convert the provided f64 value and exponent to a Distance
    pub fn from(value: f64, expo: Exponent) -> Self {
        Self(Decimal::from(value) * expo.to_base_unit())
    }

    pub fn from_base(value: f64) -> Self {
        Self::from(value, Exponent::base)
    }

    pub fn from_fraction(num: i64, denom: i64, expo: Exponent) -> Self {
        let num_u = num as u128;
        let denom_u = denom as u128;
        if (num < 0 && denom < 0) || (num > 0 && denom > 0) {
            Self(Decimal::from_fraction(Fraction::new(num_u, denom_u)) * expo.to_base_unit())
        } else {
            Self(Decimal::from_fraction(Fraction::new_neg(num_u, denom_u)) * expo.to_base_unit())
        }
    }

    /// Attempt to represent self to the provided exponent in f64
    pub fn try_to(&self, expo: Exponent) -> Result<f64, UnitError> {
        match (self.0 * expo.from_base_unit()).to_f64() {
            Some(v) => Ok(v),
            None => Err(UnitError::NotRepresentable(format!(
                "Could not convert distance represented as {} to {:?}",
                self.0, expo
            ))),
        }
    }

    /// Same as try_to, but will panic if cannot be represented as f64
    pub fn to(&self, expo: Exponent) -> f64 {
        self.try_to(expo).unwrap()
    }

    pub fn to_base(&self) -> f64 {
        self.to(Exponent::base)
    }

    pub fn decimal(&self) -> Decimal {
        self.0
    }
}

impl_dim_ops!(Distance);

impl fmt::Display for Distance {
    // Prints the Keplerian orbital elements with units
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.to(Exponent::zepto).abs() < 1.0 {
            fmt::Display::fmt(&self.to(Exponent::yocto), f)?;
            write!(f, " ym")
        } else if self.to(Exponent::atto).abs() < 1.0 {
            fmt::Display::fmt(&self.to(Exponent::zepto), f)?;
            write!(f, " zm")
        } else if self.to(Exponent::femto).abs() < 1.0 {
            fmt::Display::fmt(&self.to(Exponent::atto), f)?;
            write!(f, " am")
        } else if self.to(Exponent::pico).abs() < 1.0 {
            fmt::Display::fmt(&self.to(Exponent::femto), f)?;
            write!(f, " fm")
        } else if self.to(Exponent::nano).abs() < 1.0 {
            fmt::Display::fmt(&self.to(Exponent::pico), f)?;
            write!(f, " pm")
        } else if self.to(Exponent::micro).abs() < 1.0 {
            fmt::Display::fmt(&self.to(Exponent::nano), f)?;
            write!(f, " nm")
        } else if self.to(Exponent::milli).abs() < 1.0 {
            fmt::Display::fmt(&self.to(Exponent::micro), f)?;
            write!(f, " µm")
        } else if self.to(Exponent::centi).abs() < 10.0 {
            fmt::Display::fmt(&self.to(Exponent::milli), f)?;
            write!(f, " mm")
        } else if self.to(Exponent::base).abs() < 1.0 {
            fmt::Display::fmt(&self.to(Exponent::centi), f)?;
            write!(f, " cm")
        } else if self.to(Exponent::kilo).abs() < 10.0 {
            fmt::Display::fmt(&self.to(Exponent::base), f)?;
            // NOTE: We use meters and kilometers for a LARGE span
            // because it's super common in science
            write!(f, " m")
        } else if self.to(Exponent::giga).abs() < 1.0 {
            fmt::Display::fmt(&self.to(Exponent::kilo), f)?;
            write!(f, " km")
        } else if self.to(Exponent::tera).abs() < 1.0 {
            fmt::Display::fmt(&self.to(Exponent::giga), f)?;
            write!(f, " Gm")
        } else if self.to(Exponent::peta).abs() < 1.0 {
            fmt::Display::fmt(&self.to(Exponent::tera), f)?;
            write!(f, " Tm")
        } else if self.to(Exponent::exa).abs() < 1.0 {
            fmt::Display::fmt(&self.to(Exponent::peta), f)?;
            write!(f, " Pm")
        } else if self.to(Exponent::zetta).abs() < 1.0 {
            fmt::Display::fmt(&self.to(Exponent::exa), f)?;
            write!(f, " Em")
        } else if self.to(Exponent::yotta).abs() < 1.0 {
            fmt::Display::fmt(&self.to(Exponent::zetta), f)?;
            write!(f, " Zm")
        } else {
            fmt::Display::fmt(&self.to(Exponent::yotta), f)?;
            write!(f, " Ym")
        }
    }
}

impl fmt::LowerExp for Distance {
    // Prints the Keplerian orbital elements with units
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.to(Exponent::zepto).abs() < 1.0 {
            fmt::LowerExp::fmt(&self.to(Exponent::yocto), f)?;
            write!(f, " ym")
        } else if self.to(Exponent::atto).abs() < 1.0 {
            fmt::LowerExp::fmt(&self.to(Exponent::zepto), f)?;
            write!(f, " zm")
        } else if self.to(Exponent::femto).abs() < 1.0 {
            fmt::LowerExp::fmt(&self.to(Exponent::atto), f)?;
            write!(f, " am")
        } else if self.to(Exponent::pico).abs() < 1.0 {
            fmt::LowerExp::fmt(&self.to(Exponent::femto), f)?;
            write!(f, " fm")
        } else if self.to(Exponent::nano).abs() < 1.0 {
            fmt::LowerExp::fmt(&self.to(Exponent::pico), f)?;
            write!(f, " pm")
        } else if self.to(Exponent::micro).abs() < 1.0 {
            fmt::LowerExp::fmt(&self.to(Exponent::nano), f)?;
            write!(f, " nm")
        } else if self.to(Exponent::milli).abs() < 1.0 {
            fmt::LowerExp::fmt(&self.to(Exponent::micro), f)?;
            write!(f, " µm")
        } else if self.to(Exponent::centi).abs() < 10.0 {
            fmt::LowerExp::fmt(&self.to(Exponent::milli), f)?;
            write!(f, " mm")
        } else if self.to(Exponent::base).abs() < 1.0 {
            fmt::LowerExp::fmt(&self.to(Exponent::centi), f)?;
            write!(f, " cm")
        } else if self.to(Exponent::kilo).abs() < 10.0 {
            fmt::LowerExp::fmt(&self.to(Exponent::base), f)?;
            // NOTE: We use meters and kilometers for a LARGE span
            // because it's super common in science
            write!(f, " m")
        } else if self.to(Exponent::giga).abs() < 1.0 {
            fmt::LowerExp::fmt(&self.to(Exponent::kilo), f)?;
            write!(f, " km")
        } else if self.to(Exponent::tera).abs() < 1.0 {
            fmt::LowerExp::fmt(&self.to(Exponent::giga), f)?;
            write!(f, " Gm")
        } else if self.to(Exponent::peta).abs() < 1.0 {
            fmt::LowerExp::fmt(&self.to(Exponent::tera), f)?;
            write!(f, " Tm")
        } else if self.to(Exponent::exa).abs() < 1.0 {
            fmt::LowerExp::fmt(&self.to(Exponent::peta), f)?;
            write!(f, " Pm")
        } else if self.to(Exponent::zetta).abs() < 1.0 {
            fmt::LowerExp::fmt(&self.to(Exponent::exa), f)?;
            write!(f, " Em")
        } else if self.to(Exponent::yotta).abs() < 1.0 {
            fmt::LowerExp::fmt(&self.to(Exponent::zetta), f)?;
            write!(f, " Zm")
        } else {
            fmt::LowerExp::fmt(&self.to(Exponent::yotta), f)?;
            write!(f, " Ym")
        }
    }
}
